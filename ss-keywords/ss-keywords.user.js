// ==UserScript==
// @name          Shutterstock Keywords
// @author        nobody
// @source        https://gitlab.com/astra586/mix/-/raw/master/ss-keywords/ss-keywords.user.js
// @version       0.2
// @include       https://www.shutterstock.com/*
// ==/UserScript==

(() => {
    let path = '';
    let myId = 'my-custom-keys';

    setInterval(() => {
        if (path !== location.pathname) {
            path = location.pathname;

            if (/^\/image-(photo|vector|illustration)\//.test(path)) {
                setTimeout(() => {
                    let srcDiv = document.querySelector('div[data-automation="ExpandableKeywordsList_container_div"]');
                    if (srcDiv !== null) {
                        srcDiv.style.display = 'none';

                        let keywords = srcDiv.querySelectorAll('a'),
                            newKeywords = document.createElement('p'),
                            keywordsCount = document.createElement('span'),
                            dstDiv = document.querySelector('#'+myId);

                        if (dstDiv === null) {
                            dstDiv = document.createElement('div');
                            dstDiv.id = myId;
                            document.querySelector('div[role=presentation]').appendChild(dstDiv);
                        }

                        keywords = [...keywords].map(k => k.innerText).map(a => `<a href="/search/${a}">${a}</a>`);

                        keywordsCount.innerHTML = `${keywords.length} keywords:<br>`;

                        newKeywords.innerHTML = keywordsCount.innerHTML;
                        newKeywords.innerHTML += keywords.join(', ');
                        newKeywords.style.cssText = 'padding-left: 14px; text-align: left; line-height: 150%';

                        dstDiv.innerHTML = '';
                        dstDiv.appendChild(newKeywords);
                    }
                }, 500);
            }
        }
    }, 200);
})();
